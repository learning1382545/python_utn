Video 1: Introducción
https://www.youtube.com/watch?v=k7NSseOQSRE&t=2s

Video 2: Introducción a peewee - Primer ejemplo
https://www.youtube.com/watch?v=tUpuO1w8HR4&t=1s

Video 3: Alta de registro
https://www.youtube.com/watch?v=DuOZhH6OaFk&t=2s

Video 4: Selección de registro
https://www.youtube.com/watch?v=Dmo84eDsHXw&t=1s

Video 5: Baja y modificación de registro
https://www.youtube.com/watch?v=dRsFLMfxEQw&t=2s

Video 6: Base 2 y base 16
https://www.youtube.com/watch?v=peg9oayjFcs&t=1s

Video 7: Código de byte y bytearray
https://www.youtube.com/watch?v=QsizdNsjjRU&t=2s

Video 8: Lo que hemos visto y lo que viene
https://www.youtube.com/watch?v=yahohXj3Hpo&t=2s
