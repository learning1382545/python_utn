Video 1: Introducción
https://www.youtube.com/watch?v=W7VuJQj09Ms&feature=emb_imp_woyt

Video 2: Namespaces
https://www.youtube.com/watch?v=2V3kS6TGCzw&feature=emb_imp_woyt

Video 3: Global en módulo
https://www.youtube.com/watch?v=ipQjHgtPga4&feature=emb_imp_woyt

Video 4: Paquetes y privado
https://www.youtube.com/watch?v=1F1SieKhrkg&feature=emb_imp_woyt

Video 5: Sympy
https://www.youtube.com/watch?v=DNcsq_7KNm0&feature=emb_imp_woyt
