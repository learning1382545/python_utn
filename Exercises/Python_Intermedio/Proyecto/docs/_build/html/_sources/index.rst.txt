.. Python_Intermedio documentation master file, created by
   sphinx-quickstart on Sun Dec 11 15:12:56 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python_Intermedio's documentation!
=============================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   controlador
   exceptions
   modelo
   vista


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
