from tkinter import *
import modelo
import vista

__author__ = "Federico Capdeville"
__maintainer__ = "Federico Capdeville"
__email__ = "federicocapdeville@gmail.com"
__copyright__ = "Copyright 2022"
__version__ = "0.1"


class Control:
    """
    Main Controller class
    """
    def __init__(self, controller, my_connection):
        self.controller = controller
        self.my_connection = my_connection
        self.view = vista.View(controller, my_connection)


if __name__ == "__main__":
    try:
        my_database = modelo.DataBase()
        my_database.create_table()
    except Exception as msg:
        print(str(msg))

    root = Tk()
    Control(root, my_database)
    root.mainloop()
