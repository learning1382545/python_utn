from tkinter import *
from tkinter import ttk
import modelo


class View:
    """
    Main View class
    """
    def __init__(self, root, my_database):
        self.my_database = my_database

        root.title("Proyecto Capdeville")

        lb_title = Label(root, text="Enter events", bg="DarkOrchid3", fg="thistle1", height=1, width=60)
        lb_title.grid(row=0, column=0, columnspan=4, padx=1, pady=1, sticky=W + E)

        my_product = Label(root, text="User")
        my_product.grid(row=1, column=0, sticky=W)
        my_quantity = Label(root, text="Init day")
        my_quantity.grid(row=2, column=0, sticky=W)
        my_price = Label(root, text="End day")
        my_price.grid(row=3, column=0, sticky=W)

        a_val, b_val, c_val = StringVar(), StringVar(), StringVar()
        w_width = 20

        entry1 = Entry(root, textvariable=a_val, width=w_width)
        entry1.grid(row=1, column=1)
        entry2 = Entry(root, textvariable=b_val, width=w_width)
        entry2.grid(row=2, column=1)
        entry3 = Entry(root, textvariable=c_val, width=w_width)
        entry3.grid(row=3, column=1)

        # Treeview
        tree = ttk.Treeview(root)
        root.resizable(False, False)
        menu_bar = Menu(root)
        root.config(menu=menu_bar)

        tree["columns"] = ("col1", "col2", "col3")
        tree.column("#0", width=90, minwidth=50, anchor=W)
        tree.column("col1", width=200, minwidth=80)
        tree.column("col2", width=200, minwidth=80)
        tree.column("col3", width=200, minwidth=80)
        tree.heading("#0", text="ID")
        tree.heading("col1", text="User")
        tree.heading("col2", text="Init day")
        tree.heading("col3", text="End day")
        tree.bind("<<TreeviewSelect>>", lambda x=None: self.my_database.selected_item(tree, a_val, b_val, c_val))
        tree.grid(row=10, column=0, columnspan=4)

        # Menu - File
        file_menu = Menu(menu_bar, tearoff=0)
        file_menu.add_command(label="Export .csv", command=lambda: modelo.export_csv(self.my_database.connection))
        file_menu.add_separator()
        file_menu.add_command(label="Quit", command=lambda: modelo.exit_function())
        menu_bar.add_cascade(label="File", menu=file_menu)

        # Menu - Admin
        admin_menu = Menu(menu_bar, tearoff=0)
        admin_menu.add_command(label="Create",
                               command=lambda: self.my_database.add_value(a_val.get(), b_val.get(), c_val.get(), tree))
        admin_menu.add_command(label="Delete", command=lambda: self.my_database.delete_value(a_val.get()))
        admin_menu.add_command(label="Update", command=lambda: self.my_database.update_treeview(tree))
        admin_menu.add_command(label="Search", command=lambda: self.my_database.search(a_val.get()))
        menu_bar.add_cascade(label="Admin", menu=admin_menu)

        create_button = Button(root, text="Create",
                               command=lambda: self.my_database.add_value(a_val.get(), b_val.get(), c_val.get(), tree))
        create_button.grid(row=6, column=1)

        search_button = Button(root, text="Search", command=lambda: self.my_database.search(a_val.get()))
        search_button.grid(row=7, column=1)

        delete_button = Button(root, text="Delete", command=lambda: self.my_database.delete_value(a_val.get()))
        delete_button.grid(row=8, column=1)

