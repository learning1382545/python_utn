if __name__ == '__main__':
    exercise = int(input('Which exercise would you see?: '))

    def ex_3():
        radio = input('Input the radius of the circumference: ')
        perimeter = 2 * int(radio) * 3.1416

        return perimeter

    def ex_4():
        radio = input('Input the radius of the circumference: ')
        area = 3.1416 * int(radio) ** 2

        return area

    def ex_5():
        number = input('Insert an integer value: ')
        final_value = int(number) * 1.1

        return final_value

    if exercise == 3:
        value = ex_3()
        print('Perimeter is: ' + str(value))
    elif exercise == 4:
        value = ex_4()
        print('Area is: ' + str(value))
    elif exercise == 5:
        value = ex_5()
        print('Value incremented 10% is: ' + str(value))
    else:
        print('Wrong number!! Choose 3, 4 or 5')
