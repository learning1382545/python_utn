from tkinter import *

background = 1

# Create the window
root = Tk()
root.title("Unit 5 - Challenge")
root.geometry("250x150")
root.configure(background="blue")


def print_terminal():
    print('Name: ' + complete1.get() + '\nLast name: ' + complete2.get() + '\nAge: ' + complete3.get())


def background_color():
    global background
    if background:
        root.configure(background="red")
        textTitle.configure(background="blue")
        textTitle.configure(foreground="red")
        background = 0
    else:
        root.configure(background="blue")
        textTitle.configure(background="red")
        textTitle.configure(foreground="blue")
        background = 1


textTitle = Label(root, text="INSERT DATA...", background="red", foreground="blue", anchor=CENTER, font=("Arial", 15))
textTitle.place(x=50, y=10)
text1 = Label(root, text="Name")
text1.place(x=20, y=50)
text2 = Label(root, text="Last Name")
text2.place(x=20, y=70)
text3 = Label(root, text="Age")
text3.place(x=20, y=90)

complete1 = Entry(root)
complete1.place(x=100, y=50)
complete2 = Entry(root)
complete2.place(x=100, y=70)
complete3 = Entry(root)
complete3.place(x=100, y=90)

save_button = Button(root, text="Save changes", command=print_terminal)
save_button.place(x=20, y=120)

color_button = Button(root, text="Change color", command=background_color)
color_button.place(x=140, y=120)

# Maintain the window open
root.mainloop()
