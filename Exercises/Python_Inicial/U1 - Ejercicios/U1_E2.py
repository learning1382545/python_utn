import sys

if __name__ == '__main__':
    print('This is the exercise 2.\n')

    if len(sys.argv) != 4:
        print('Quantity of values is not correct. It should be 3 numbers.')
        exit()

    print('First parameter:' + str(sys.argv[1]))
    if int(sys.argv[1]) % 2 == 0:
        print('Divisible by 2')
    else:
        print('Not divisible by 2')
    print()

    print('Second parameter:' + str(sys.argv[1]))
    if int(sys.argv[2]) % 2 == 0:
        print('Divisible by 2')
    else:
        print('Not divisible by 2')
    print()

    print('Third parameter:' + str(sys.argv[1]))
    if int(sys.argv[3]) % 2 == 0:
        print('Divisible by 2')
    else:
        print('Not divisible by 2')
