import sys

if __name__ == '__main__':
    print('This is the exercise 1.\n')

    if len(sys.argv) != 4:
        print('Quantity of values is not correct. It should be 3 numbers.')
        exit()

    print('arg1 * arg2 + arg3')
    print(int(sys.argv[1]) * int(sys.argv[2]) + int(sys.argv[3]))
