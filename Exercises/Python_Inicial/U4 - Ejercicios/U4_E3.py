def add(value1, value2):
    res = value1 + value2
    return res


if __name__ == '__main__':
    user_number_1 = int(input('Insert number 1 to check: '))
    user_number_2 = int(input('Insert number 2 to check: '))
    variable = lambda number1, number2: number1 + number2

    print('Addition: ' + str(variable(user_number_1, user_number_2)))
