def order(my_list):
    my_list.sort()
    return my_list


if __name__ == '__main__':
    list_to_order = [3, 44, 21, 78, 5, 56, 9]

    list_to_order = order(list_to_order)

    print('List ordered: ' + str(list_to_order))
