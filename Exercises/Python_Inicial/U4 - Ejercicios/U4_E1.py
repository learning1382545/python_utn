if __name__ == '__main__':
    user_number = int(input('Insert number to check: '))
    variable = lambda number: number % 2

    if variable(user_number):
        print('Number is not divisible by 2')
    else:
        print('Number is divisible by 2')
