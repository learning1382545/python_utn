def all_numbers(age):
    for counter in range(1, age+1):
        print(str(counter), end='')
        if counter == age:
            continue
        else:
            print(',', end=' ')

    print()

    for counter2 in range(age, 0, -1):
        print(str(counter2), end='')
        if counter2 == 1:
            return
        else:
            print(',', end=' ')

    return


if __name__ == '__main__':
    user_age = int(input('Insert user age: '))

    all_numbers(user_age)
