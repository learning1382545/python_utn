def multiple_by_3(value):
    res = 3 * value
    return res


if __name__ == '__main__':
    user_number = int(input('Insert number to check: '))
    variable = lambda number: number * 3

    print('Number multiplied by 3: ' + str(variable(user_number)))
