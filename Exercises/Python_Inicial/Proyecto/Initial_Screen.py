from tkinter import *

# Create the window
root = Tk()
root.title("Pantalla inicial")
root.geometry("300x150")
root.resizable(False, False)


label_title = Label(root, text="Ingrese usuario y contraseña", anchor=CENTER, font=("Arial", 15))
label_title.place(x=10, y=10)
label_user = Label(root, text="Usuario: ")
label_user.place(x=20, y=50)
label_pass = Label(root, text="Contraseña: ")
label_pass.place(x=20, y=70)

complete1 = Entry(root)
complete1.place(x=100, y=50)
complete2 = Entry(root)
complete2.place(x=100, y=70)

button_next = Button(root, text="OK")
button_next.place(x=60, y=100)

button_cancel = Button(root, text="Cancelar")
button_cancel.place(x=130, y=100)

# Maintain the window open
root.mainloop()
