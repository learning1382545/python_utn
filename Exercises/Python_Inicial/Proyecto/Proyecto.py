from tkinter import *
import sqlite3
from tkinter import ttk
import re

# ######################################################################################################################
#                                                       MODEL
# ######################################################################################################################


def export_csv():
    cursor = connection.cursor()
    sql = "SELECT * FROM project;"
    cursor.execute(sql,)
    connection.commit()
    lines = cursor.fetchall()

    with open("export_csv.csv", "w") as fp:
        fp.write("ID;User;Initial day;End day")
        fp.write("\n")
        for line in lines:
            fp.write(str(line[0]) + ";" + str(line[1]) + ";" + str(line[2]))
            fp.write("\n")


def selected_item(user, init, end):
    for element in tree.selection():
        item = list(tree.item(element, "values"))
        user.set(item[0])
        init.set(item[1])
        end.set(item[2])


def exit_function():
    exit()


def db_connect():
    my_connection = sqlite3.connect('mi_database.db')
    return my_connection


def create_table(my_connection):
    cursor = my_connection.cursor()
    sql = """CREATE TABLE project
             (id INTEGER PRIMARY KEY AUTOINCREMENT,
             user varchar(20) NOT NULL,
             init_day varchar(20) NOT NULL,
             end_day varchar(20) NOT NULL)
    """

    cursor.execute(sql)
    my_connection.commit()


def add_value(user, init_day, end_day, my_tree):
    patron = "^[A-Za-záéíóú]*$"  # Regex

    if re.match(patron, user):
        con = db_connect()
        cursor = con.cursor()
        data = (user, init_day, end_day)
        sql = "INSERT INTO project (user, init_day, end_day) VALUES(?, ?, ?)"
        cursor.execute(sql, data)
        con.commit()
        update_treeview(my_tree)
    else:
        print("Failure due to regex")


def search(my_user):
    cursor = connection.cursor()
    data = (my_user, )
    sql = "SELECT * FROM project WHERE user = ?;"
    cursor.execute(sql, data)
    connection.commit()
    print(cursor.fetchall())


def search_id(my_id):
    cursor = connection.cursor()
    data = (my_id, )
    sql = "SELECT * FROM project WHERE id = ?;"
    cursor.execute(sql, data)
    connection.commit()


def delete_value(my_user):
    con = db_connect()
    cursor = con.cursor()
    data = (my_user,)
    sql = "DELETE FROM project WHERE user = ?;"
    cursor.execute(sql, data)
    con.commit()


def update_treeview(my_treeview):
    records = my_treeview.get_children()
    for element in records:
        my_treeview.delete(element)

    sql = "SELECT * FROM project ORDER BY id ASC"
    con = db_connect()
    cursor = con.cursor()
    data = cursor.execute(sql)

    result = data.fetchall()
    for element in result:
        print(element)
        my_treeview.insert("", 0, text=element[0], values=(element[1], element[2], element[3]))

# ######################################################################################################################
#                                                       VIEW
# ######################################################################################################################


try:
    connection = db_connect()
    create_table(connection)
except:
    print("There is a mistake generated")


root = Tk()
root.title("Proyecto Capdeville")

lb_title = Label(root, text="Enter events", bg="DarkOrchid3", fg="thistle1", height=1, width=60)
lb_title.grid(row=0, column=0, columnspan=4, padx=1, pady=1, sticky=W+E)

my_product = Label(root, text="User")
my_product.grid(row=1, column=0, sticky=W)
my_quantity = Label(root, text="Init day")
my_quantity.grid(row=2, column=0, sticky=W)
my_price = Label(root, text="End day")
my_price.grid(row=3, column=0, sticky=W)

a_val, b_val, c_val = StringVar(), StringVar(), StringVar()
w_width = 20

entry1 = Entry(root, textvariable=a_val, width=w_width)
entry1.grid(row=1, column=1)
entry2 = Entry(root, textvariable=b_val, width=w_width)
entry2.grid(row=2, column=1)
entry3 = Entry(root, textvariable=c_val, width=w_width)
entry3.grid(row=3, column=1)

# ######################################################################################################################
#                                                       TREEVIEW
# ######################################################################################################################

tree = ttk.Treeview(root)
root.resizable(False, False)
menu_bar = Menu(root)
root.config(menu=menu_bar)

tree["columns"] = ("col1", "col2", "col3")
tree.column("#0", width=90, minwidth=50, anchor=W)
tree.column("col1", width=200, minwidth=80)
tree.column("col2", width=200, minwidth=80)
tree.column("col3", width=200, minwidth=80)
tree.heading("#0", text="ID")
tree.heading("col1", text="User")
tree.heading("col2", text="Init day")
tree.heading("col3", text="End day")
tree.bind("<<TreeviewSelect>>", lambda x=None: selected_item(a_val, b_val, c_val))
tree.grid(row=10, column=0, columnspan=4)

# Menu - File
file_menu = Menu(menu_bar, tearoff=0)
file_menu.add_command(label="Export .csv", command=lambda: export_csv())
file_menu.add_separator()
file_menu.add_command(label="Quit", command=lambda: exit_function())
menu_bar.add_cascade(label="File", menu=file_menu)

# Menu - Admin
admin_menu = Menu(menu_bar, tearoff=0)
admin_menu.add_command(label="Create", command=lambda: add_value(a_val.get(), b_val.get(), c_val.get(), tree))
admin_menu.add_command(label="Delete", command=lambda: delete_value(a_val.get()))
admin_menu.add_command(label="Update", command=lambda: update_treeview(tree))
admin_menu.add_command(label="Search", command=lambda: search(a_val.get()))
menu_bar.add_cascade(label="Admin", menu=admin_menu)

create_button = Button(root, text="Create", command=lambda: add_value(a_val.get(), b_val.get(), c_val.get(), tree))
create_button.grid(row=6, column=1)

search_button = Button(root, text="Search", command=lambda: search(a_val.get()))
search_button.grid(row=7, column=1)

delete_button = Button(root, text="Delete", command=lambda: delete_value(a_val.get()))
delete_button.grid(row=8, column=1)
root.mainloop()
