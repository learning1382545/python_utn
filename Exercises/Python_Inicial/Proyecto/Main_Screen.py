from tkinter import *

# Create the window
root = Tk()
root.title("Pantalla principal")
root.geometry("350x350")
root.resizable(False, False)
menu_bar = Menu(root)
root.config(menu=menu_bar)

# Barra del menu - File
file_menu = Menu(menu_bar, tearoff=0)
file_menu.add_command(label="Exportar .csv")
file_menu.add_separator()
file_menu.add_command(label="Salir")
menu_bar.add_cascade(label="Archivo", menu=file_menu)

# Barra del menu - Administrador
admin_menu = Menu(menu_bar, tearoff=0)
admin_menu.add_command(label="Alta")
admin_menu.add_command(label="Baja")
admin_menu.add_command(label="Modificar")
admin_menu.add_command(label="Consultar existentes")
menu_bar.add_cascade(label="Administrador", menu=admin_menu)

# Barra del menu - Personal
user_menu = Menu(menu_bar, tearoff=0)
user_menu.add_command(label="Alta")
user_menu.add_command(label="Baja")
user_menu.add_command(label="Modificar")
user_menu.add_command(label="Consultar existentes")
menu_bar.add_cascade(label="Personal", menu=user_menu)

# Barra del menu - Informacion
info_menu = Menu(menu_bar, tearoff=0)
info_menu.add_command(label="Eventos por día")
menu_bar.add_cascade(label="Información", menu=info_menu)

# Distribución de la pantalla principal
button_event = Button(root, text="Ingresar Evento", font=("Arial", 15), padx=85)
button_event.place(x=20, y=10)

label_user = Label(root, text="Usuario: ")
label_user.place(x=20, y=60)
label_init = Label(root, text="Día inicio: ")
label_init.place(x=20, y=80)
label_end = Label(root, text="Día final: ")
label_end.place(x=20, y=100)

entry_user = Entry(root)
entry_user.place(x=100, y=60)
entry_init = Entry(root)
entry_init.place(x=100, y=80)
entry_end = Entry(root)
entry_end.place(x=100, y=100)

button_query = Button(root, text="Consulta", font=("Arial", 15), pady=11)
button_query.place(x=250, y=60)

listbox_excel = Listbox(root, width=53)
listbox_excel.place(x=20, y=140)
listbox_excel.insert(0, "Display info")

# Maintain the window open
root.mainloop()
