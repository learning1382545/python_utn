if __name__ == '__main__':
    is_continue = True
    shopping = {}
    item = 1

    while is_continue:
        quantity = input('Insert quantity: ')
        price = input('Insert price: ')
        shopping[item] = [quantity, price]

        next_shopping = input('Anything else? Y/N: ')
        next_shopping = next_shopping.lower()
        item += 1

        if next_shopping == 'n':
            is_continue = False

    print('Full shopping list: ' + str(shopping))
