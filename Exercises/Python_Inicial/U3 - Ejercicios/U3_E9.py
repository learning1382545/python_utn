def alta(f_quantity, f_price, f_shopping, f_item):
    f_shopping[f_item] = [f_quantity, f_price]
    return f_shopping


def baja(f_shopping, f_item):
    f_shopping.pop(f_item)
    return f_shopping


def consulta():
    print('Full shopping list: ' + str(shopping))
    return


def modificar(f_quantity, f_price, f_shopping, f_item):
    f_shopping = alta(f_quantity, f_price, f_shopping, f_item)
    return f_shopping


if __name__ == '__main__':
    is_continue = True
    shopping = {}
    item = 1

    while is_continue:
        quantity = input('Insert quantity: ')
        price = input('Insert price: ')
        shopping = alta(quantity, price, shopping, item)

        next_shopping = input('Anything else? B(baja)/C(consulta)/M(modificar)/Y/N: ')
        next_shopping = next_shopping.lower()

        if next_shopping == 'c':
            consulta()
            next_shopping = input('Anything else? Y/N: ')
            next_shopping = next_shopping.lower()

        if next_shopping == 'm':
            quantity = input('Insert modified quantity: ')
            price = input('Insert modified price: ')
            modificar(quantity, price, shopping, item)
            next_shopping = input('Anything else? Y/N: ')
            next_shopping = next_shopping.lower()

        if next_shopping == 'b':
            shopping = baja(shopping, item)
            next_shopping = input('Anything else? Y/N: ')
            next_shopping = next_shopping.lower()

        if next_shopping == 'n':
            break

        item += 1

    print('Full shopping list: ' + str(shopping))

# Pregunta: Considera que es más fácil guardar la información en listas o diccionarios?
# Respuesta: considero que son dos herramientas útiles. Que ambas pueden ser usadas y que son similares en cuanto a
# su accionamiento. No considero una más fácil que otra, pero sí una más útil que otra dependiendo el caso.
