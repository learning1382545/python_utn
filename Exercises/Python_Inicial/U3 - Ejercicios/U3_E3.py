if __name__ == '__main__':
    counter = 0

    phrase = str(input('Insert a phrase: '))
    phrase = phrase.lower()

    for element in phrase:
        if element == 'a' or element == 'e' or element == 'i' or element == 'o' or element == 'u':
            counter += 1
        if element == 'á' or element == 'é' or element == 'í' or element == 'ó' or element == 'ú':
            counter += 1

    print("Quantity of 'vowels' in the phrase: " + str(counter))
