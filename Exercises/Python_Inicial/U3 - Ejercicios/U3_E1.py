if __name__ == '__main__':
    x = 0

    while x < 3:
        value = int(input('Insert the number: '))
        if value % 2 == 0:
            print('Divisible by 2')
        else:
            print('Not divisible by 2')
        x += 1
