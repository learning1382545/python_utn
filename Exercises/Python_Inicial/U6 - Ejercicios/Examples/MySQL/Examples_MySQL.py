"""

How to open database visualizer: https://www.youtube.com/watch?v=ac7K3OHStPg
Create database and tables: https://www.youtube.com/watch?v=0_d1YEoM0V4
Insert and delete data from table: https://www.youtube.com/watch?v=0eQCokw_f5c
Select and update data from table: https://www.youtube.com/watch?v=pdIN1A2UWUI

install before running:
pip install mysql-connector
pip install pymysql
pip install mysqlclient
pip install mysql-connector-python
"""

import mysql.connector


def create_db():
    """Create database if not exist

    :return:
    """
    my_db2 = mysql.connector.connect(
        host="localhost",
        user="root",
        password=""
    )

    cursor = my_db2.cursor()
    cursor.execute("CREATE DATABASE my_database")


def create_table():
    """Create table if not exist

    :return:
    """
    my_db2 = mysql.connector.connect(
        host="localhost",
        user="root",
        password="",
        database="my_database"
    )

    cursor = my_db2.cursor()
    cursor.execute("CREATE TABLE my_table(id int(7) NOT NULL PRIMARY KEY AUTO_INCREMENT, title VARCHAR(128))")


def insert_in_table(database):
    """Insert data in table

    :return:
    """
    cursor = database.cursor()
    sql = "INSERT INTO my_table (title) VALUES (%s)"
    data = ("Product1", )
    cursor.execute(sql, data)
    database.commit()


def delete_in_table(database):
    """Delete data in table

    :return:
    """
    cursor = database.cursor()
    sql = "DELETE FROM my_table WHERE id = %s"
    data = ("2", )
    cursor.execute(sql, data)
    database.commit()


def select_in_table(database):
    """Select data from table

    :return:
    """
    cursor = database.cursor()
    sql = "SELECT * FROM my_table"
    cursor.execute(sql)
    return cursor.fetchall()


def update_in_table(database):
    """Update data in table

    :return:
    """
    cursor = database.cursor()
    sql = "UPDATE my_table SET title = %s WHERE id = %s;"
    data = ("Modification", 3)
    cursor.execute(sql, data)
    database.commit()


# Create database in MyQSL (if not exist)
# create_db()

# Create table in a database
# create_table()

my_db = mysql.connector.connect(
    host="localhost",
    user="root",
    password="",
    database="my_database"
)

# Insert data in table
# insert_in_table(my_db)

# Delete data in table
# delete_in_table(my_db)

# Select data from table
"""rows = select_in_table(my_db)
for row in rows:
    print(row)
"""

# Update data in table
update_in_table(my_db)
