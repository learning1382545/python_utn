"""Videos associated as tutorials

How to create a database, create a table, insert, delete and update data in a database:
 https://www.youtube.com/watch?v=ix7Jc3Moc_U
How to select data from a database: https://www.youtube.com/watch?v=aL3vDJOKJQI
"""

import sqlite3


def create_db():
    """Create if not exist

    :return:
    """
    connection = sqlite3.connect('mi_database.db')
    return connection


def create_table(connection):
    """Create if not exist

    :param connection:
    :return:
    """
    cursor = connection.cursor()
    sql = "CREATE TABLE personas (id integer PRIMARY KEY, name text)"
    cursor.execute(sql)
    connection.commit()


def insert_in_table(connection, my_id, name):
    """Insert data in table

    :param connection:
    :param my_id:
    :param name:
    :return:
    """
    cursor = connection.cursor()
    data = (my_id, name)
    sql = "INSERT INTO personas (id, name) VALUES (?, ?);"
    cursor.execute(sql, data)
    connection.commit()


def delete_in_table(connection, my_id):
    """Delete data in table

    :param connection:
    :param my_id:
    :return:
    """
    cursor = connection.cursor()
    data = (my_id, )
    sql = "DELETE FROM personas WHERE id = ?;"
    cursor.execute(sql, data)
    connection.commit()


def update_in_table(connection, my_id, name):
    """Update data in database

    :param connection:
    :param my_id:
    :param name:
    :return:
    """
    cursor = connection.cursor()
    data = (name, my_id)
    sql = "UPDATE personas SET name = ? WHERE id = ?;"
    cursor.execute(sql, data)
    connection.commit()


def select_in_table(connection, my_id):
    """Select element by id in a table

    :param connection:
    :param my_id:
    :return:
    """
    cursor = connection.cursor()
    data = (my_id, )
    sql = "SELECT * FROM personas WHERE id = ?;"
    cursor.execute(sql, data)
    connection.commit()
    return cursor.fetchall()


# Connect to the database
con = create_db()

# Create a table (if not exist)
# create_table(con)

# Insert data to a table
# insert_in_table(con, 3, "Ezequiel")

# Delete from a table checking by id
# delete_in_table(con, 3)

# Update data in a table
# update_in_table(con, 2, "Modified")

# Select data from a table
rows = select_in_table(con, 2)

# Print on screen every row
for row in rows:
    print(row)
