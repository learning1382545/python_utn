import os
from log_file import *


class RegisterLogError(Exception):
    """
    Main Exceptions class

    It contains only one exception associated to the register error.
    Other exceptions are associated to the default Exception class
    """

    BASE_DIR = os.path.dirname((os.path.abspath(__file__)))
    path = os.path.join(BASE_DIR, "log.txt")

    @my_log("\tRegisterLogError - init")
    def __init__(self, message):
        self.message = message
        self.date = datetime.datetime.now()

    @my_log("\tRegister as a log in a file the exception occurred")
    def reg_error(self):
        """
        Register as a log in a file the exception occurred

        :return: None
        """
        my_log = open(self.path, "a")
        print(self.message, self.date, file=my_log)
