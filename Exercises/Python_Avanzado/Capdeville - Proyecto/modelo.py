import sqlite3
import re
import exceptions
from log_file import *


class DataBase:
    """
    Main Modelo class
    """

    @my_log("\tDataBase - init")
    def __init__(self):
        self.connection = self.db_connect()

    @my_log("\tConnects to the data base in use")
    def db_connect(self):
        """
        Connects to the database in use

        :return: sqlite3.connect class
        """

        my_connection = sqlite3.connect('mi_database.db')
        return my_connection

    @my_log("Create a table in a database if not exists")
    def create_table(self):
        """
        Create a table in a database if not exists

        :return: None
        """
        cursor = self.connection.cursor()
        sql = """CREATE TABLE project
                 (id INTEGER PRIMARY KEY AUTOINCREMENT,
                 user varchar(20) NOT NULL UNIQUE,
                 init_day varchar(20) NOT NULL,
                 end_day varchar(20) NOT NULL)
        """

        cursor.execute(sql)
        self.connection.commit()

    @my_log("\tSearch by id the item associated")
    def search_id(self, my_id):
        """
        Search by id the item associated

        :param my_id: id of the item to search

        :return: Items
        """
        cursor = self.connection.cursor()
        data = (my_id, )
        sql = "SELECT * FROM project WHERE id = ?;"
        cursor.execute(sql, data)
        self.connection.commit()

    @my_log("\t\tSearch by user the item associated")
    def search(self, my_user):
        """
        Search by user the item associated

        :param my_user: user of the item to search

        :return: Items
        """
        cursor = self.connection.cursor()
        data = (my_user, )
        sql = "SELECT * FROM project WHERE user = ?;"
        cursor.execute(sql, data)
        self.connection.commit()
        print(cursor.fetchall())

    @my_log("Sets items in the treeview used in view module")
    def selected_item(self, tree, user, init, end):
        """
        Sets items in the treeview used in view module

        :param tree: Tree to use

        :param user: Variable associated to the user

        :param init: Variable associated to the init day

        :param end: Variable associated to the end day

        :return: no variable - visual in the treeview
        """
        for element in tree.selection():
            item = list(tree.item(element, "values"))
            user.set(item[0])
            init.set(item[1])
            end.set(item[2])

    @my_log("Delete an item in the database")
    def delete_value(self, my_user):
        """
        Delete an item in the database

        :param my_user: user to delete in the database

        :return: None
        """
        con = self.db_connect()
        cursor = con.cursor()
        data = (my_user,)
        sql = "DELETE FROM project WHERE user = ?;"
        cursor.execute(sql, data)
        con.commit()
        print("Element has been removed successfully: " + str(data[0]))

    @my_log("Updates treeview in tkinter")
    def update_treeview(self, my_treeview):
        """
        Updates treeview in tkinter

        :param my_treeview: treeview associated to the view

        :return: None - is visual
        """
        records = my_treeview.get_children()
        for element in records:
            my_treeview.delete(element)

        sql = "SELECT * FROM project ORDER BY id ASC"
        con = self.db_connect()
        cursor = con.cursor()
        data = cursor.execute(sql)

        result = data.fetchall()
        for element in result:
            print(element)
            my_treeview.insert("", 0, text=element[0], values=(element[1], element[2], element[3]))

    @my_log("\tAdd value in the database and update treeview associated")
    def add_value(self, user, init_day, end_day, my_tree):
        """
        Add value in the database and update treeview associated

        :param user: Variable associated to the user

        :param init_day: Variable associated to the init day

        :param end_day: Variable associated to the end day

        :param my_tree: Variable associated to the treeview

        :return: None - database and visual changes
        """
        patron = "^[A-Za-záéíóú]*$"  # Regex

        try:
            if re.match(patron, user):
                # Try to save in database the value associated
                try:
                    con = self.db_connect()
                    cursor = con.cursor()
                    data = (user, init_day, end_day)
                    sql = "INSERT INTO project (user, init_day, end_day) VALUES(?, ?, ?)"
                    cursor.execute(sql, data)
                    con.commit()
                    self.update_treeview(my_tree)
                except Exception as log:
                    print("[ERROR] User shall not be duplicated")
                    print(log)

            else:
                print("[ERROR] Value shall only include letters (Regex)")
                raise exceptions.RegisterLogError("Failure due to regex ==> user:" + str(user) + " ---")
        except exceptions.RegisterLogError as log:
            log.reg_error()


@my_log("\tExport csv file")
def export_csv(connection):
    """
    Export csv file

    :param connection: connection to the database

    :return: Output - csv file
    """
    cursor = connection.cursor()
    sql = "SELECT * FROM project;"
    cursor.execute(sql,)
    connection.commit()
    lines = cursor.fetchall()

    with open("export_csv.csv", "w") as fp:
        fp.write("ID;User;Initial day;End day")
        fp.write("\n")
        for line in lines:
            fp.write(str(line[0]) + ";" + str(line[1]) + ";" + str(line[2]))
            fp.write("\n")


@my_log("A function that finishes the program")
def exit_function():
    """
    A function that finishes the program

    :return: Bye!
    """
    exit()
