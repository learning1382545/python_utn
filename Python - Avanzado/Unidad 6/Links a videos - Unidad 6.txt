Video 1: queue FIFO – LIFO - PRIORITY
https://www.youtube.com/watch?v=5Oe2fbTeKvY&t=2s

Video 2: thread
https://www.youtube.com/watch?v=7SbpW64wE1k&t=1s

Video 2.1: thread con queue
https://www.youtube.com/watch?v=-pcCuqH7eh4&t=2s

Video 3: subrutina
https://www.youtube.com/watch?v=0Tvq_DRTxb8&t=2s 

Video 4: Números binarios y hexadecimales
https://www.youtube.com/watch?v=NyA0LUIy4aY&t=1s

Video 5: Modelo OSI y TCP/IP
https://www.youtube.com/watch?v=o2inLbEfEzc&t=7s

Video 6: SOCKET y SOCKET CON THREAD
https://www.youtube.com/watch?v=2R4buen58PI&t=1s
