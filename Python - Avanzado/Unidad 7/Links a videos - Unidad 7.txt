Video 1: Introducción
https://www.youtube.com/watch?v=vgEPgQGGrMo&t=1s

Video 2: Generadores
https://www.youtube.com/watch?v=hL85sKIsx8g&t=1s

Video 3: Corrutinas I
https://www.youtube.com/watch?v=ajN_1j0gmME&t=1s

Video 4: Corrutinas II
https://www.youtube.com/watch?v=fToLzSozH8A&t=1s

Video 5: Catching
https://www.youtube.com/watch?v=50OasK-N2f8&t=1s

Video 6: Logging
https://www.youtube.com/watch?v=IpOc9QsinjQ&t=1s

Video 7: Futuros
https://www.youtube.com/watch?v=xmsVKt4XJLk&t=1s

Video 8: Primer ejemplo con asyncio
https://www.youtube.com/watch?v=intUxMQrJq4&t=1s
