Video 1: Extender funcionalidad de clase
https://www.youtube.com/watch?v=jL4kdlVxIhM&t=1s

Video 2: La clase como objeto de type
https://www.youtube.com/watch?v=-a38Uhk6Xp0&t=1s

Video 3: Tiempo de ejecución, __new__ y __init__
https://www.youtube.com/watch?v=x0n4Bg0iV5c&t=5s

Video 4: Acceso a atributos y métodos de metaclase
https://www.youtube.com/watch?v=-0iOfKKGHFs&t=1s

Video 5: Métodos y atributos de instancia desde metaclase
https://www.youtube.com/watch?v=_Xh1x3CiBhI&t=1s

Video 6: Herencia de metaclases
https://www.youtube.com/watch?v=sHNstppqli8&t=1s

Video 7: Sobrecarga en metaclases
https://www.youtube.com/watch?v=C83kz5wiFTM&t=1s
