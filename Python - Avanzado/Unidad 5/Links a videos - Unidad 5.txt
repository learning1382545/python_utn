Video 1: Patrón observador
https://www.youtube.com/watch?v=zuQVlLu_2iM&t=1s

Video 2: Patrón observador aplicación
https://www.youtube.com/watch?v=pt6Bfyozlyo&t=1s

Video 3: Patrón adaptador
https://www.youtube.com/watch?v=m6ytIB7KC1c&t=1s

Video 4: Patrón singleton
https://www.youtube.com/watch?v=ARD5G3QB6Lo&t=2s

Video 5: Patrón factory
https://www.youtube.com/watch?v=saLiPsCJNPA&t=1s
